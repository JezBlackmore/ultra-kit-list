const scaleFunction = () => {

// Remove the transition class
const square = document.getElementById('ultraHeader2');
square.classList.remove('square-animation');

// Create the observer, same as before:
const observer = new IntersectionObserver(entries => {
  entries.forEach(entry => {
    if (entry.isIntersecting) {
        setTimeout(() => {
            square.classList.add('square-animation');
        }, 0.5)
     
      return;
    }

    square.classList.remove('square-animation');
  });
});

observer.observe(document.getElementById('ultraHeader2'));





// Remove the transition class
const square2 = document.getElementById('ultraHeader3');
square2.classList.remove('square-animation');

// Create the observer, same as before:
const observer2 = new IntersectionObserver(entries => {
  entries.forEach(entry => {
    if (entry.isIntersecting) {
      square2.classList.add('square-animation');
      return;
    }

    square2.classList.remove('square-animation');
  });
});

observer2.observe(document.getElementById('ultraHeader3'));




// Remove the transition class
const square3 = document.getElementById('ultraHeader4');
square3.classList.remove('square-animation');

// Create the observer, same as before:
const observer3 = new IntersectionObserver(entries => {
  entries.forEach(entry => {
    if (entry.isIntersecting) {
      square3.classList.add('square-animation');
      return;
    }

    square3.classList.remove('square-animation');
  });
});

observer3.observe(document.getElementById('ultraHeader4'));



// Remove the transition class
const square4 = document.getElementById('ultraHeader5');
square4.classList.remove('square-animation');

// Create the observer, same as before:
const observer4 = new IntersectionObserver(entries => {
  entries.forEach(entry => {
    if (entry.isIntersecting) {
      square4.classList.add('square-animation');
      return;
    }

    square4.classList.remove('square-animation');
  });
});

observer4.observe(document.getElementById('ultraHeader5'));


}

export default scaleFunction;
