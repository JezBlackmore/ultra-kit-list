
const scriptsJs =() => {
    /* console.log("test") */
    /* import products from './products.js'; */
    /* import trackLocation from './compare.js'; */
    let startIndex = 0;
    let pressed = false;
    let touched = false;
    let startx;
    let x;
    
    /* let startIndex1 = 0;
    let pressed1 = false;
    let touched1 = false;
    let startx1;
    let x1;
    
    let startIndex2 = 0;
    let pressed2 = false;
    let touched2 = false;
    let startx2;
    let x2;
    
    let startIndex3 = 0;
    let pressed3 = false;
    let touched3 = false;
    let startx3;
    let x3;
    
    let startIndex4 = 0;
    let pressed4 = false;
    let touched4 = false;
    let startx4;
    let x4;
    
    let startIndex5 = 0;
    let pressed5 = false;
    let touched5 = false;
    let startx5;
    let x5;
    
    let startIndex6 = 0;
    let pressed6 = false;
    let touched6 = false;
    let startx6;
    let x6; */
    
    /* const html = (products) => {
        const productsCopy = [...products]
        let html = '';
    
        productsCopy.forEach(product => {
    
            html += `<div class="slider_card">
            ${product.feature ? `<div class="feature"><span>${product.feature}</span></div>`: `<div class="noFeature"></div>`}
            <div class="slider_card_img">
                <img class="sliderImage" src="${product.image}" alt="${product.title}">
            </div>
            <div class="slider_card_meta">
                <div  class="slider_card_meta_info">
                    <h4>${product.title}</h4>
                    ${!product.onSale ? `<p>£${product.price}.00</p>`: `<p><span class="lineThroughText">£${product.price}.00</span> £${product.salePrice}.00</p>`}
                </div>
                <div class="slider_card_meta_actions">
                    <a href="${product.linkMen}">Shop Men</a>
                    <a href="${product.linkWomen}">Shop Women</a>
                </div>
            </div>
            </div>`
        })
    
        return html;
    } */
    
    /* document.getElementById('slider').innerHTML = html(products); */
    
    
    const slider1 = document.querySelector('.sider_container1');
    const innerSlider1 = document.querySelector('.innerSlider1');
    const progressBarInner1 = document.querySelector('.progressBarInner1');
    
    const slider1Links = document.querySelectorAll('a');
    
    const slider2 = document.querySelector('.sider_container2');
    const innerSlider2 = document.querySelector('.innerSlider2');
    const progressBarInner2 = document.querySelector('.progressBarInner2');
    
    const slider3 = document.querySelector('.sider_container3');
    const innerSlider3 = document.querySelector('.innerSlider3');
    const progressBarInner3 = document.querySelector('.progressBarInner3');
    
    const slider4 = document.querySelector('.sider_container4');
    const innerSlider4 = document.querySelector('.innerSlider4');
    const progressBarInner4 = document.querySelector('.progressBarInner4');

    
    document.querySelectorAll('.sliderImage').forEach(image => {
        image.setAttribute('draggable', false);
    })
    
    
    /* leftArrow.addEventListener('click', () => {
        startIndex--;
        updatePage(products);
    })
    
    rightArrow.addEventListener('click', () => {
        startIndex++;
        updatePage(products);
    }) */
    
    progressBarInner1.addEventListener('mousedown', (e) => {
        console.log("click")
          pressed = true;
          startx = e.offsetX - innerSlider1.offsetLeft;
          progressBarInner1.style.cursor = 'grabbing';
      })
      progressBarInner1.addEventListener('mouseenter', (e) => {
        progressBarInner1.style.cursor = 'grab';
    })
    progressBarInner1.addEventListener('mouseup', (e) => {
        progressBarInner1.style.cursor = 'grab';
    })
    progressBarInner1.addEventListener('mouseup', (e) => {
        pressed = false;
    })
    progressBarInner1.addEventListener('mousemove', (e) => {
        if (!pressed) return;
        e.preventDefault();
        x = e.offsetX;
        innerSlider1.style.left = `${x - startx}px`;
       
        checkboundary(slider1, innerSlider1);
       progress(slider1, innerSlider1, progressBarInner1); 
    })
    
    
    
    slider1.addEventListener('mousedown', (e) => {
      /*   e.preventDefault();
        e.stopPropagation(); 
     */
        pressed = true;
        startx = e.offsetX - innerSlider1.offsetLeft;
        slider1.style.cursor = 'grabbing';
    })
    slider1.addEventListener('mouseenter', (e) => {
        slider1.style.cursor = 'grab';
    })
    slider1.addEventListener('mouseup', (e) => {
        slider1.style.cursor = 'grab';
    })
    
    window.addEventListener('mouseup', (e) => {
        pressed = false;
    })
    slider1.addEventListener('mousemove', (e) => {
        if (!pressed) return;
        e.preventDefault();
        x = e.offsetX;
        innerSlider1.style.left = `${x - startx}px`;
       
        checkboundary(slider1, innerSlider1);
       progress(slider1, innerSlider1, progressBarInner1); 
    })
    /* ############## */
    
    slider2.addEventListener('mousedown', (e) => {
        pressed = true;
        startx = e.offsetX - innerSlider2.offsetLeft;
        slider2.style.cursor = 'grabbing';
    
    })
    slider2.addEventListener('mouseenter', () => {
        slider2.style.cursor = 'grab';
    })
    slider2.addEventListener('mouseup', () => {
        slider2.style.cursor = 'grab';
    })
    
    window.addEventListener('mouseup', () => {
        pressed = false;
    })
    slider2.addEventListener('mousemove', (e) => {
        if (!pressed) return;
        e.preventDefault();
        x = e.offsetX;
        innerSlider2.style.left = `${x - startx}px`;
        checkboundary(slider2, innerSlider2);
        progress(slider2, innerSlider2, progressBarInner2);
    })
    
    /* ############## */
    
    
    /* ############## */
    
    slider3.addEventListener('mousedown', (e) => {
        pressed = true;
        startx = e.offsetX - innerSlider3.offsetLeft;
        slider3.style.cursor = 'grabbing';
    
    })
    slider3.addEventListener('mouseenter', () => {
        slider3.style.cursor = 'grab';
    })
    slider3.addEventListener('mouseup', () => {
        slider3.style.cursor = 'grab';
    })
    
    window.addEventListener('mouseup', () => {
        pressed = false;
    })
    slider3.addEventListener('mousemove', (e) => {
        if (!pressed) return;
        e.preventDefault();
        x = e.offsetX;
        innerSlider3.style.left = `${x - startx}px`;
        checkboundary(slider3, innerSlider3);
        progress(slider3, innerSlider3, progressBarInner3);
    })
    
    /* ############## */
    /* ############## */
    
    slider4.addEventListener('mousedown', (e) => {
        pressed = true;
        startx = e.offsetX - innerSlider4.offsetLeft;
        slider4.style.cursor = 'grabbing';
    
    })
    slider4.addEventListener('mouseenter', () => {
        slider4.style.cursor = 'grab';
    })
    slider4.addEventListener('mouseup', () => {
        slider4.style.cursor = 'grab';
    })
    
    window.addEventListener('mouseup', () => {
        pressed = false;
    })
    slider4.addEventListener('mousemove', (e) => {
        if (!pressed) return;
        e.preventDefault();
        x = e.offsetX;
        innerSlider4.style.left = `${x - startx}px`;
        checkboundary(slider4, innerSlider4);
        progress(slider4, innerSlider4, progressBarInner4);
    })

    
    
    /* ############## */
    
    
    
    slider1.addEventListener('touchstart', (e) => {
        touched = true;
        startx = e.targetTouches[0].pageX - innerSlider1.offsetLeft;
        slider1.style.cursor = 'grabbing';
    })
    /* slider.addEventListener('mouseenter', () => {
        slider.style.cursor = 'grab';
    }) */
    /* slider.addEventListener('touchend', () => {
        slider.style.cursor = 'grab';
    }) */
    
    window.addEventListener('touchend', () => {
        touched = false;
    })
    slider1.addEventListener('touchmove', (e) => {
       /*  if (!touched) return;
        e.preventDefault(); */
        x = e.targetTouches[0].pageX;
        innerSlider1.style.left = `${x - startx}px`;
        checkboundary(slider1, innerSlider1);
        progress(slider1, innerSlider1, progressBarInner1);
    }) 
    
    /* ############## */
    
    
    
    slider2.addEventListener('touchstart', (e) => {
        touched = true;
        startx = e.targetTouches[0].pageX - innerSlider2.offsetLeft;
        slider2.style.cursor = 'grabbing';
    })
    /* slider.addEventListener('mouseenter', () => {
        slider.style.cursor = 'grab';
    }) */
    /* slider.addEventListener('touchend', () => {
        slider.style.cursor = 'grab';
    }) */
    
    slider2.addEventListener('touchmove', (e) => {
       /*  if (!touched) return;
        e.preventDefault(); */
        x = e.targetTouches[0].pageX;
        innerSlider2.style.left = `${x - startx}px`;
        checkboundary(slider2, innerSlider2);
        progress(slider2, innerSlider2, progressBarInner2);
    }) 
    
    
    /* ############## */
    
    
    
    slider3.addEventListener('touchstart', (e) => {
        touched = true;
        startx = e.targetTouches[0].pageX - innerSlider3.offsetLeft;
        slider3.style.cursor = 'grabbing';
    })
    /* slider.addEventListener('mouseenter', () => {
        slider.style.cursor = 'grab';
    }) */
    /* slider.addEventListener('touchend', () => {
        slider.style.cursor = 'grab';
    }) */
    
    
    slider3.addEventListener('touchmove', (e) => {
       /*  if (!touched) return;
        e.preventDefault(); */
        x = e.targetTouches[0].pageX;
        innerSlider3.style.left = `${x - startx}px`;
        checkboundary(slider3, innerSlider3);
        progress(slider3, innerSlider3, progressBarInner3);
    }) 
    
    
    /* ############## */
    
    
    
    slider4.addEventListener('touchstart', (e) => {
        touched = true;
        startx = e.targetTouches[0].pageX - innerSlider4.offsetLeft;
        slider4.style.cursor = 'grabbing';
    })
    /* slider.addEventListener('mouseenter', () => {
        slider.style.cursor = 'grab';
    }) */
    /* slider.addEventListener('touchend', () => {
        slider.style.cursor = 'grab';
    }) */
    
    slider4.addEventListener('touchmove', (e) => {
       /*  if (!touched) return;
        e.preventDefault(); */
        x = e.targetTouches[0].pageX;
        innerSlider4.style.left = `${x - startx}px`;
        checkboundary(slider4, innerSlider4);
        progress(slider4, innerSlider4, progressBarInner4);
    }) 
    
    
    
    const checkboundary = (slider, innerSlider) => {
        let outer = slider.getBoundingClientRect();
        let inner = innerSlider.getBoundingClientRect();
    
        if (parseInt(innerSlider.style.left) > 0) {
            innerSlider.style.left = '0px';
        } else if (inner.right < outer.right) {
            innerSlider.style.left = `-${inner.width - outer.width}px`;
        }
    }
    
    /* const progress = (slider, innerSlider, progressBarInner) => {
        let outer = slider.getBoundingClientRect();
        let inner = innerSlider.getBoundingClientRect();
        const percent100 = inner.right;
        const percent1 = percent100 / 100;
        let currentPercent =  Math.round(outer.right / percent1);
        const widthToNumber = progressBarInner.style.width.match(/\d/g);
        const numb = widthToNumber.join("");
      progressBarInner.style.left = `${currentPercent - +numb}%`;
    } */
    
    
    const progress = (slider, innerSlider, progressBarInner) => {
        let outer = slider.getBoundingClientRect();
        let inner = innerSlider.getBoundingClientRect();
        const percent100 = inner.right;
        const percent1 = percent100 / 100;
        let currentPercent =  Math.round(outer.right / percent1);
      progressBarInner.style.width = `${currentPercent}%`;
    }
    
    /* let test = document.querySelector('#video-compare-container');
    
    test.addEventListener("mouseenter", function( event ) {
        function trackLocation(e) {
            var rect = videoContainer.getBoundingClientRect(),
                position = ((e.pageX - rect.left) / videoContainer.offsetWidth)*100;
            if (position <= 100) { 
              videoClipper.style.width = position+"%";
              clippedVideo.style.width = ((100/position)*100)+"%";
              clippedVideo.style.zIndex = 3;
              }
          }
          var videoContainer = document.getElementById("video-compare-container"),
          videoClipper = document.getElementById("video-clipper"),
          clippedVideo = videoClipper.getElementsByTagName("video")[0];
          videoContainer.addEventListener( "mousemove", trackLocation, false); 
          videoContainer.addEventListener("touchstart",trackLocation,false);
          videoContainer.addEventListener("touchmove",trackLocation,false);
          
        
    }); */
    
    progress(slider1, innerSlider1, progressBarInner1);
    progress(slider2, innerSlider2, progressBarInner2);
    progress(slider3, innerSlider3, progressBarInner3);
    progress(slider4, innerSlider4, progressBarInner4);
 
    
    
    }
    
    export default scriptsJs;